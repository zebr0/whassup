import http.server
import os
import socketserver
import threading
import time

import pytest


@pytest.fixture(scope="module")
def server():
    class Server(http.server.ThreadingHTTPServer):
        def __init__(self):
            super().__init__(("0.0.0.0", 8000), socketserver.BaseRequestHandler)

        def __enter__(self):
            threading.Thread(target=self.serve_forever).start()
            return self

        def __exit__(self, exc_type, exc_val, exc_tb):
            self.shutdown()
            self.server_close()

    with Server() as server:
        yield server


@pytest.fixture(autouse=True)
def timezone():
    os.environ["TZ"] = "UTC"
    time.tzset()
